﻿using Microsoft.AspNetCore.Components;
using System;
using System.Threading.Tasks;

namespace AppDemo.Pages
{
    public class CounterBase : ComponentBase, IDisposable
    {


        [Parameter]
        public int InitialCount { get; set; }

        protected bool Loading { get; set; }

        protected string Color { get; private set; }


        protected int currentCount = 0;


        protected string ButtonType = "btn btn-primary";

        protected void IncrementCount()
        {
            currentCount++;

            this.ButtonType = currentCount > 5 ? "btn btn-danger" : "btn btn-warning";

            this.Color = currentCount > 20 ? "red" : "green";

        }


        protected override void OnInitialized()
        {

            Console.WriteLine("OnInitialized BEGIN");

            Loading = true;

            _ = Task.Delay(5000).ContinueWith(x => { this.Loading = false; InvokeAsync(StateHasChanged); });

            base.OnInitialized();

            Console.WriteLine("OnInitialized END");
        }


        protected override void OnParametersSet()
        {

            currentCount = InitialCount;
            Console.WriteLine("OnParameterset BEGIN");

            base.OnParametersSet();

            Console.WriteLine("OnParameterSet END");
        }


        protected override void OnAfterRender(bool firstRender)
        {
            Console.WriteLine("OnAfterRender BEGIN");
            Console.WriteLine(firstRender);
            base.OnAfterRender(firstRender);
            Console.WriteLine("OnAfterRender END");
        }

        protected override bool ShouldRender()
        {
            Console.WriteLine("ShouldRender BEGIN");
            Console.WriteLine("ShouldRender END");
            return base.ShouldRender();

        }

        public void Dispose()
        {
            Console.WriteLine("Dispose");
        }



    }
}
