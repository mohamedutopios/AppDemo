﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppDemo.Models
{
    public class Customer
    {

        [Required]
        public string Name { get; set; }

        [Required, RegularExpression("^[0-9]*$")]
        public int Number { get; set; }

        [Required, StringLength(10, MinimumLength = 1, ErrorMessage = "Le nombre de caractère minimum est de 1 et le maximum est de 20")]
        public string Street { get; set; }

        [Required, RegularExpression("^[0-9]{5}$")]
        public string ZipCode { get; set; }





    }
}
