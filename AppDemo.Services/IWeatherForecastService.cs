﻿using AppDemo.Models;
using System;
using System.Threading.Tasks;

namespace AppDemo.Services
{
    public interface IWeatherForecastService
    {
        Task<WeatherForecast[]> GetForecastAsync(DateTime startDate);
    }
}